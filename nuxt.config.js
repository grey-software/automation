require("dotenv").config();
export default {
  serverMiddleware: [{ path: "/", handler: "~/api/server.js" }],
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "Automation Dashboard",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/tailwindcss",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "nuxt-clipboard",
    "@nuxtjs/toast",
    "@nuxtjs/dotenv",
    "@nuxtjs/axios",
    "@nuxt/content",
    "@nuxtjs/moment",
  ],

  axios: {
    browserBaseURL: `http://localhost:${process.env.SERVER_PORT}`,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config, { isDev, isClient }) {
      config.node = { fs: "empty" };
    },
  },
};
