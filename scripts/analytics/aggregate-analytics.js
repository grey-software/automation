const axios = require("axios");
require("dotenv").config();
const apiUrl = "https://plausible.io/api/v1/stats/aggregate";

const aggregateAnalytics = (period, key) => {
  const apiOptions = `period=${period}&metrics=visitors,pageviews`;
  const sites = [
    "grey.software",
    "org.grey.software",
    "resources.grey.software",
    "learn.grey.software",
    "glossary.grey.software",
    "material-math.grey.software",
  ];

  const siteAnalytics = {
    total: { pageViews: 0, uniqueVisits: 0 },
  };

  sites.forEach(async (site) => {
    const analyticsResult = await axios.get(
      `${apiUrl}?site_id=${site}&${apiOptions}`,
      {
        headers: {
          Authorization: `Bearer ${key}`,
        },
      }
    );
    const pageViews = analyticsResult.data.results.pageviews.value;
    const uniqueVisits = analyticsResult.data.results.visitors.value;
    const analytics = {
      pageViews,
      uniqueVisits,
    };
    siteAnalytics.total.pageViews += pageViews;
    siteAnalytics.total.uniqueVisits += uniqueVisits;
    siteAnalytics[site] = analytics;
  });

  return siteAnalytics;
};

module.exports = aggregateAnalytics;
