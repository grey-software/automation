const fs = require("fs");

const generateSponsorStats = (sponsors) => {
  let cumulativeTotal = 0;
  let monthlyTotal = 0;

  for (const sponsor of sponsors) {
    cumulativeTotal += sponsor.totalAmount;
    monthlyTotal += sponsor.monthlyAmount;
  }

  return {
    cumulativeTotal,
    monthlyTotal,
  };
};

module.exports = { generateSponsorStats };
