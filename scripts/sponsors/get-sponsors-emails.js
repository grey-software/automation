const fs = require("fs");
let sponsorsReportFilePath = process.argv[2]

if (sponsorsReportFilePath === undefined) {
    console.log()
    console.log("Sponsor Report File not Specified")
    console.log()
    return
}

function getDate(dateString) {
    let date = new Date(dateString);
    let options = { month: "long" };
    let month = new Intl.DateTimeFormat("en-US", options).format(date);
    return `${month} ${date.getDate()}, ${date.getFullYear()}`;
}

function writeSponsorFile(path, formattedSponsors) {
    fs.writeFileSync(path, JSON.stringify({ sponsors: formattedSponsors }, null, 2))
}

function isActiveSponsor(lastTransactionDate) {
    const monthBeforeToday = new Date(new Date().setDate(new Date().getDate() - 31))
    console.log(lastTransactionDate)
    console.log(monthBeforeToday)
    console.log(monthBeforeToday <= lastTransactionDate)
    return monthBeforeToday <= lastTransactionDate
}

fs.readFile(sponsorsReportFilePath, (err, data) => {
    if (err) throw err;
    const reportSponsors = JSON.parse(data);
    const formattedSponsors = reportSponsors
        .filter(sponsor => sponsor.sponsor_public_email)
        .filter(sponsor => isActiveSponsor(new Date(sponsor.transactions[0].transaction_date)))
        .map(reportSponsor => {
            let transactions = reportSponsor.transactions;
            const endDate = new Date(transactions[0].transaction_date)
            if (isActiveSponsor(endDate))
                return {
                    name: reportSponsor.sponsor_profile_name,
                    email: reportSponsor.sponsor_public_email,
                    startDate: getDate(reportSponsor.sponsorship_started_on),
                    endDate: getDate(transactions[0].transaction_date)
                }
        });

    writeSponsorFile('./sponsors-emails.json', formattedSponsors)
});


