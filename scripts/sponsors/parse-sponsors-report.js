const fs = require("fs");
const {
  writeFormattedJsonFile,
} = require("../utilities/write_formatted_json_file");

function getDate(dateString) {
  let date = new Date(dateString);
  let options = { month: "long" };
  let month = new Intl.DateTimeFormat("en-US", options).format(date);
  return `${month} ${date.getDate()}, ${date.getFullYear()}`;
}

const parseSponsorReport = (sponsorReport) => {
  const formattedSponsors = sponsorReport.map((reportSponsor) => {
    let transactions = reportSponsor.transactions;
    let monthlyAmount = reportSponsor.transactions[0].tier_monthly_amount;
    let totalAmount = transactions.reduce((amount, transaction) => {
      return amount + Number(transaction.processed_amount.replace("$", ""));
    }, 0);

    const formattedSponsor = {
      monthlyAmount: Number(monthlyAmount.replace("$", "")),
      totalAmount: Math.floor(totalAmount),
      startDate: getDate(reportSponsor.sponsorship_started_on),
      endDate: getDate(transactions[0].transaction_date),
    };

    let username = reportSponsor.is_public
      ? reportSponsor.sponsor_handle
      : "Private";
    return { username: username, ...formattedSponsor };
  });
  writeFormattedJsonFile("content/sponsors.json", formattedSponsors);
  return formattedSponsors;
};

module.exports = { parseSponsorReport };
