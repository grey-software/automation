const fs = require("fs");
const shell = require("shelljs");
const path = require("path");
require("dotenv").config();

const folderPath = `${process.env.GREYSOFT_PROJECTS_PATH}`;
const gitlabPrivateKey = process.env.GITLAB_PRIVATE_KEY;
const gitlabPrivateKeyName = process.env.GITLAB_PRIVATE_KEY_NAME;
const landingWebsiteRepoUrl = "https://gitlab.com/grey-software/website";

const updateRepoFile = (RepoUrl, dataToUpdate, remotePath, commit) => {
  const splitRepoArray = RepoUrl.split("/");
  const repoName = splitRepoArray.pop();

  shell.cd(folderPath);

  if (!fs.existsSync(path.resolve(repoName))) {
    shell.exec(`git clone ${landingWebsiteRepoUrl}.git`);
  }

  shell.cd(repoName);
  shell.exec(
    `git remote add myrepo https://${gitlabPrivateKeyName}:${gitlabPrivateKey}@gitlab.com/grey-software/${repoName}`
  );

  shell.exec("git fetch");
  shell.exec("git checkout myrepo master");
  shell.exec("git pull -f");

  fs.writeFile(
    `${folderPath}${repoName}${remotePath}`,
    JSON.stringify(dataToUpdate, null, 2),
    function (err) {
      if (err) throw err;
    }
  );
  const branchName = `updated-${remotePath}-${new Date().toLocaleDateString()}`;
  shell.exec(`git checkout -b ${branchName}`);
  shell.exec("git add --a");
  shell.exec(`git commit -m ${commit}`);
  shell.exec(
    `git push -o merge_request.create --set-upstream myrepo ${branchName}`
  );

  shell.exec("git remote rm myrepo");
};

module.exports = { updateRepoFile };
