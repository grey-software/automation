const fs = require("fs");
export const writeFormattedJsonFile = (path, formattedSponsors) => {
  fs.writeFile(
    path,
    JSON.stringify({ sponsors: formattedSponsors }, null, 2),
    function (err) {
      if (err) {
        console.log(err);
      }
    }
  );
};
