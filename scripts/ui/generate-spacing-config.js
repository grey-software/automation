const fs = require("fs");
const path = require("path");

const sortStringByNumericalValue = (stringA, stringB) => {
  return Number(stringA) - Number(stringB);
};

const generateSpacingConfig = () => {
  let spacingConfig = {};
  let remValue = 0;
  let nameValue = 0;

  while (nameValue < 4) {
    nameValue = nameValue + 0.5;
    remValue += 0.125;
    spacingConfig[nameValue] = remValue;
  }

  while (nameValue < 64) {
    nameValue += 1;
    remValue += 0.25;
    spacingConfig[nameValue] = remValue;
  }

  while (nameValue < 128) {
    nameValue += 2;
    remValue += 0.5;
    spacingConfig[nameValue] = remValue;
  }

  let config = [];
  Object.keys(spacingConfig)
    .sort(sortStringByNumericalValue)
    .forEach((key) => {
      const configLine = `'${key}': '${spacingConfig[key]}rem', // ${
        spacingConfig[key] * 16
      }px \n`;
      config.push(configLine);
    });

  let spacingConfigToStrig = config.join("");
  spacingConfigToStrig = "export default {" + spacingConfigToStrig + "} \n";

  return spacingConfigToStrig;
};



module.exports = { generateSpacingConfig, sortStringByNumericalValue };
