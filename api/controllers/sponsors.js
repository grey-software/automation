import { parseSponsorReport } from "../../scripts/sponsors/parse-sponsors-report";
import { generateSponsorStats } from "../../scripts/sponsors/generate-sponsors-stats";

const SponsorReport = (req, reply) => {
  const formattedSponsors = parseSponsorReport(req.body);
  reply.send(formattedSponsors);
};

const getSponsorStats = (req, reply) => {
  const sponsorStats = generateSponsorStats(req.body);
  reply.send(sponsorStats);
};

module.exports = {
  SponsorReport,
  getSponsorStats,
};
