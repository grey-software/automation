const { updateRepoFile } = require("../../scripts/utilities/update-repo-file");
const shell = require("shelljs");

const updateRepo = (req, reply) => {
  const request = req.body;
  console.log(request);
  repos = request.repoUrl;
  repos.forEach((repo) => {
    updateRepoFile(
      repo,
      request.dataToUpdate,
      request.remotePath,
      request.commit
    );
  });

  return { massage: "File updated" };
};

module.exports = { updateRepo };
