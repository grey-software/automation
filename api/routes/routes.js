import { updateRepo } from "../controllers/repos";
import { SponsorReport, getSponsorStats } from "../controllers/sponsors";

function sponsorRoutes(fastify, options, done) {
  // Get all items
  fastify.post("/sponsors", SponsorReport);
  fastify.post("/stats", getSponsorStats);
  fastify.put("/repo", updateRepo);
  done();
}

module.exports = sponsorRoutes;
