const fastify = require("fastify")({
  logger: true,
});
fastify.register(require("fastify-cors"), {
  origin: true,
});

require("dotenv").config();
fastify.register(require("./routes/routes"));

const PORT = process.env.SERVER_PORT;

const start = async () => {
  try {
    await fastify.listen(PORT);
  } catch (error) {
    fastify.log.error(error);
    process.exit(1);
  }
};

start();

module.exports = { path: "/", handler: fastify };
